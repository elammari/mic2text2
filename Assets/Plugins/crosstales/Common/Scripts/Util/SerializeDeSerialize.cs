﻿using UnityEngine;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Crosstales.Common.Util
{
    /// <summary>Serialize and deserialize objects to/from binary files.</summary>
    public partial class SerializeDeSerialize<T>
    {
        #region Serialization
        public void ToFile(T o, string path)
        {
            try
            {
                FileStream FS;
                using (FS = new FileStream(path, FileMode.Create, FileAccess.Write))
                {
                    BinaryFormatter binaryFormatter = new BinaryFormatter();
                    binaryFormatter.Serialize(FS, o);
                }
            }
            catch (Exception ex)
            {
                Debug.LogError("Could not save serialized file: " + ex);
            }
        }

        public MemoryStream ToMemory(T o)
        {
            MemoryStream memoryStream = new MemoryStream();
            BinaryFormatter BF = new BinaryFormatter();
            BF.Serialize(memoryStream, o);
            return memoryStream;
        }

        public Byte[] ToByteArray(T o)
        {
            Byte[] arr;
            MemoryStream memoryStream;
            using (memoryStream = new MemoryStream())
            {
                BinaryFormatter BF = new BinaryFormatter();
                BF.Serialize(memoryStream, o);
                arr = memoryStream.ToArray();
            }
            return arr;
        }
        #endregion


        #region Deserialization
        public T FromFile(string path)
        {

            T o = default(T);

            try
            {
                FileStream fileStream;
                using (fileStream = new FileStream(path, FileMode.Open))
                {
                    BinaryFormatter BF = new BinaryFormatter();
                    o = (T)BF.Deserialize(fileStream);
                }
            }
            catch (Exception ex)
            {
                Debug.LogError("Could not load serialized file: " + ex);
            }
            return o;
        }

        public T FromMemory(Byte[] data)
        {
            T o;
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            using (MemoryStream memoryStream = new MemoryStream(data))
            {
                o = (T)binaryFormatter.Deserialize(memoryStream);
            }
            return o;
        }
        #endregion
    }
}
// © 2017-2018 crosstales LLC (https://www.crosstales.com)